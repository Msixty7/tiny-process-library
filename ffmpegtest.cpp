#include "process.hpp"
#include <iostream>

using namespace std;
using namespace TinyProcessLib;

int main() {
#if !defined(_WIN32) || defined(MSYS_PROCESS_USE_SH)

  //
  // The following examples will fail when executing from commandline on macOS
  //
  Config cfg;
  cfg.buffer_size = 1024;

  std::string command = "/usr/local/bin/ffmpeg -i '" MEDIA_TEST_FILES "/video.mp4' -i '" MEDIA_TEST_FILES "/audio.m4a' -c:v copy -map 0:v:0 -map 1:a:0 -y '" MEDIA_TEST_FILES "/result.mp4'";

  auto outCb = [&](const char* bytes, size_t n)
  {
	  string tmp;
	  tmp.assign(bytes, n);
	  cerr << tmp;
  };

  Process process(command, "", outCb, nullptr, false, cfg);
#endif
}
